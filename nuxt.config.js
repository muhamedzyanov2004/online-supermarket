module.exports = {
  /*
  ** Headers of the page
  */
  plugins: ['~plugins/bootstrap-vue'],
  router: {
    linkActiveClass: 'active',
    linkExactActiveClass: 'active',
  },
  css: [
    './assets/scss/bootstrap.scss' // use our build, as entered via app.scss
  ],
  head: {
    title: 'online-supermarket',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
 loading: { color: '#3B8070' },
 /*
 ** Build configuration
 */
  modules: [
    '@nuxtjs/axios',
  ],

  axios: {
    // proxyHeaders: false
  },
  /*
  ** Build configuration
  */
  build: {
    vendor: ['bootstrap-vue'],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

