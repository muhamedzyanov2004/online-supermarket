import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { PaginationPlugin } from 'vuex-pagination'

Vue.use(Vuex);
Vue.use(PaginationPlugin);

const store = () => new Vuex.Store({
    state: {
        products: [],
        cart: [],
        favourite: []
    },
    mutations: {
        SET_PRODUCTS_TO_STATE: (state, products) => {
            state.products = products;
        },
        SET_CART: (state, product) => {
            console.log(state.cart.length)
            if (state.cart.length) {
                let isProductExists = false;
                state.cart.map(function(item, index) {
                    if (item.id === product.id) {
                        isProductExists = true;
                        item.counter++
                    }
                })
                if(!isProductExists) {
                    state.cart.push(product)
                }
            } else {
                state.cart.push(product)
                console.log(state.products)
            }
        },
        REMOVE_FROM_CART: (state, index) => {
            state.cart.splice(index, 1)
        },

        SET_FAVOURITE: (state, product) => {
          if (state.favourite.length) {
              let isProductExists = false;
              state.favourite.map(function(item) {
                  if (item.id === product.id) {
                      isProductExists = true;
                      item.counter++
                  }
              })
              if(!isProductExists) {
                  state.favourite.push(product)
              }
          } else {
              state.favourite.push(product)
          }
        },
        REMOVE_FROM_FAVOURITE: (state, index) => {
            state.favourite.splice(index, 1)
        }
    },
    actions: {
        GET_PRODUCTS_FROM_API({commit}) {
            return axios('/cards.json', {
                method: "GET"
            })
            .then(products => {
                commit('SET_PRODUCTS_TO_STATE', products.data);
                return products;
            })
            .catch((error) => {
                console.log(error)
                return error;
            })
        },
        ADD_TO_CART({commit}, product) {
            commit('SET_CART', product);
        },
        DELETE_FROM_CART({commit}, index) {
            commit('REMOVE_FROM_CART', index);
        },

        ADD_TO_FAVOURITE({commit}, product) {
          commit('SET_FAVOURITE', product);
        },
        DELETE_FROM_FAVOURITE({commit}, index) {
            commit('REMOVE_FROM_FAVOURITE', index);
        }
    },
    getters: {
        PRODUCTS(state) {
            return state.products;
        },
        CART(state) {
            return state.cart;
        },
        FAVOURITE(state) {
            return state.favourite;
        }
    }
})

export default store;
